/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Menu;
import examen.Recibo;
import java.sql.*;

/**
 *
 * @author PC
 */
public class dbRecibo {
    
    private String MYSQLDRIVER="com.mysql.cj.jdbc.Driver";
    private String MYSQLDB ="jdbc:mysql://3.132.136.208:3306/marcosm?user=marcosm&password=12345";
    private Connection conexion;
    private String strConsulta;
    private ResultSet registros;
    
    
    //constructor
    
    public dbRecibo(){
        try{
            Class.forName(MYSQLDRIVER);
        
        
        }catch(ClassNotFoundException e){
        
        System.out.println("surgio un error"+e.getMessage());
        System.exit(-1);
            
        }
        
    }
    
    public void conectar(){
        try{
        conexion=DriverManager.getConnection(MYSQLDB);
        } catch(SQLException e){
            System.out.println("no se loggo conectar"+ e.getMessage());
        }
    }
    
    public void desconectar(){
        
        try{
        conexion.close();
        }catch(SQLException e){
        
        System.out.println("surgio un error al desconectar"+e.getMessage());
        }
    }
    
    public void insertar(Recibo rec){
        conectar();
        try{
            strConsulta="INSERT INTO recibo(numrecibo,fecha,nombre,domicilio,tipo,costo,consumo,status)"+" VALUES(?,?,?,?,?,?,?,?)";
            
            PreparedStatement pst=conexion.prepareStatement(strConsulta);
            pst.setInt(1,rec.getNumrecibo());
            pst.setString(2,rec.getFecha());
            pst.setString(3,rec.getNombre());
            pst.setString(4,rec.getDomicilio());
            pst.setInt(5,rec.getTiposervicio());
            pst.setFloat(6,rec.getCostopork());
            pst.setFloat(7, (float) rec.getKilowattsc());
            pst.setInt(8,rec.getStatus());
            
            pst.executeUpdate();
        }catch (SQLException e){
            System.out.println("error al insertar"+ e.getMessage());
        }
        desconectar();
        
    }
    
     public void actualizar(Recibo rec){
         Recibo recibo=new Recibo();
         
         strConsulta="UPDATE recibo SET nombre=?,domicilio=?,fecha=?,tipo="+"?,costo=? WHERE numrecibo=? and status=0;";
     this.conectar();
     try{
         PreparedStatement pst=conexion.prepareStatement(strConsulta);
         
            pst.setString(1,rec.getNombre());
            pst.setString(2,rec.getDomicilio());
            pst.setString(3,rec.getFecha());
            pst.setInt(4,rec.getTiposervicio());
            pst.setFloat(5,rec.getCostopork());
            pst.setFloat(6, (float) rec.getKilowattsc());
            pst.setInt(7,rec.getNumrecibo());
            
            
            pst.executeUpdate();
            this.desconectar();
     }catch (SQLException e){
         System.err.println("Surgio un error al actualizar: "+ e.getMessage());
     }
     }
     
     public void habilitar(Recibo rec){
         strConsulta="UPDATE recibo SET status =0 WHERE numrecibo=?";
         this.conectar();
         try{
             System.err.println("se conecto");
             PreparedStatement pst=conexion.prepareStatement(strConsulta);
             
             pst.setInt(1, rec.getNumrecibo());
             pst.executeUpdate();
             this.desconectar();
         }catch (SQLException e){
             System.err.println("surgio un error al habilitar"+ e.getMessage());
         }
     }
     
     public void deshabilitar(Recibo rec){
         
         strConsulta="UPDATE recibo SET status=1 WHERE numrecibo=?";
         this.conectar();
         try
         {
             System.err.println("Se conecto");
             PreparedStatement pst=conexion.prepareStatement(strConsulta);
             
             pst.setInt(1,rec.getNumrecibo());
             
             pst.executeUpdate();
             this.desconectar();
             
         }
         catch(SQLException e ){
             System.err.println("Surgio un error"+ e.getMessage());
     }
         
     }
     
     public boolean isExiste(int numrecibo,int status){
         boolean exito=false;
         this.conectar();
         strConsulta="SELECT*FROM recibo WHERE numrecibo=? and status=?;";
         try{
             PreparedStatement pst=conexion.prepareStatement(strConsulta);
             pst.setInt(1,numrecibo);
             pst.setInt(2, status);
             this.registros=pst.executeQuery();
             if(this.registros.next()) exito=true;
         }
         catch(SQLException e){
             System.out.println("surgio un error al verificar si existe"+ e.getMessage());
         }
         this.desconectar();
         return exito;
     }
     
     public Recibo buscar(int numrecibo){
         Recibo recibo=new Recibo();
         conectar();
         try{
             strConsulta="SELECT * FROM recibo WHERE numrecibo=? and status=0;";
             PreparedStatement pst=conexion.prepareStatement(strConsulta);
             
             pst.setInt(1,numrecibo);
             this.registros=pst.executeQuery();
             if(this.registros.next()){
                 recibo.setId(registros.getInt("id"));
                 recibo.setNumrecibo(registros.getInt("numrecibo"));
                 recibo.setNombre(registros.getString("nombre"));
                 recibo.setDomicilio(registros.getString("domicilio"));
                 recibo.setTiposervicio(registros.getInt("tipo"));
                 recibo.setCostopork((float)registros.getDouble("costo"));
                 recibo.setKilowattsc((float) registros.getDouble("consumo"));
                 recibo.setFecha(registros.getString("fecha"));
                 
             }else recibo.setId(0);
         }
         catch(SQLException e){
             System.out.println("Surgio un error al habilitar:"+ e.getMessage());
         }
         this.desconectar();
         return recibo;
     }
      
}
