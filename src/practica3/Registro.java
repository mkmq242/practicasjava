/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practica3;

/**
 *
 * @author REX//
 */
public class Registro {
    
    private int CodigoP;
    private String Descripcion;
    private String UnidadM;
    private float PrecioC;
    private float PrecioV;
    private int CantidadP;
 
 
    
    public Registro(){
    this.CodigoP=0;
    this.Descripcion="";
    this.UnidadM="";
    this.PrecioC=0.0f;
    this.PrecioV=0.0f;
    this.CantidadP=0;

    
}
    
    public Registro(int Codigo,String Descipcion,String UnidadM,float PrecioC,float PrecioV,int CantidadP,float CalcularPVT,float CalcularPCT,float Ganancia,float PrecioVT,float PrecioCT){
    
    this.CodigoP=CodigoP;
    this.Descripcion=Descripcion;
    this.UnidadM=UnidadM;
    this.PrecioC=PrecioC;
    this.PrecioV=PrecioV;
    this.CantidadP=CantidadP;
   
    
    
    }
    

    public int getCodigoP() {
        return CodigoP;
    }

    public void setCodigoP(int CodigoP) {
        this.CodigoP = CodigoP;
    }

    public String getDescripcion() {
        return Descripcion;
    }

    public void setDescripcion(String Descripcion) {
        this.Descripcion = Descripcion;
    }

    public String getUnidadM() {
        return UnidadM;
    }

    public void setUnidadM(String UnidadM) {
        this.UnidadM = UnidadM;
    }

    public float getPrecioC() {
        return PrecioC;
    }

    public void setPrecioC(float PrecioC) {
        this.PrecioC = PrecioC;
    }

    public float getPrecioV() {
        return PrecioV;
    }

    public void setPrecioV(float PrecioV) {
        this.PrecioV = PrecioV;
    }

    public int getCantidadP() {
        return CantidadP;
    }

    public void setCantidadP(int CantidadP) {
        this.CantidadP = CantidadP;
    }
    
    //calcular metodos
    
    public float CalcularPVT(){
        float calcularPVT=0.0f;
       calcularPVT=PrecioV*CantidadP;
        
        return calcularPVT;
    
}
      public float CalcularPCT(){
      float calcularPCT=0.0f;
       calcularPCT=PrecioC*CantidadP;
        
        return calcularPCT;
    
}
      
      public float Ganancia(){
          float PrecioVT=0.0f;
          float PrecioCT=0.0f;
          float Ganancia=0.0f;
          
          PrecioCT=PrecioC*CantidadP;
          PrecioVT=PrecioV*CantidadP;
          Ganancia=PrecioVT-PrecioCT;
        return Ganancia;
          
      }
      
    
    
}