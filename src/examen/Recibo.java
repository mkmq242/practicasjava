/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examen;

/**
 *
 * @author PC
 */
public class Recibo {
    private int id;
    private int status;
     public int numrecibo;
    public String fecha;
    public String nombre;
    public String domicilio;
    public int tiposervicio;
    public float costopork;

    public float kilowattsc;
    
    public Recibo(){
    this.id=0;
    this.numrecibo=0;
    this.fecha="";
    this.nombre="";
    this.domicilio="";
    this.tiposervicio =0;
    this. costopork = 0.0f;
    this.kilowattsc=0.0f;
    
    this.status=0;
        
    }
    
       public Recibo(int id,int numrecibo,String fecha , String nombre, String domicilio, int tiposervicio, float costopork, float kilowattsc,int status){
    this.id=id;
    this.numrecibo=numrecibo;
    this.fecha=fecha;
    this.nombre=nombre;
    this.domicilio=domicilio;
    this.tiposervicio =tiposervicio;
    this. costopork = costopork;
    this.kilowattsc=kilowattsc;
    this.status=status;
    }
           public Recibo(Recibo otro){
    this.id=otro.id;
    this.numrecibo=otro.numrecibo;
    this.fecha=otro.fecha;
    this.nombre=otro.nombre;
    this.domicilio=otro.domicilio;
     this.tiposervicio =otro.tiposervicio;
    this. costopork = otro.costopork;
    this.kilowattsc=otro.kilowattsc;
    this.status=otro.status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getNumrecibo() {
        return numrecibo;
    }

    public void setNumrecibo(int numrecibo) {
        this.numrecibo = numrecibo;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    public int getTiposervicio() {
        return tiposervicio;
    }

    public void setTiposervicio(int tiposervicio) {
        this.tiposervicio = tiposervicio;
    }

    public float getCostopork() {
        return costopork;
    }

    public void setCostopork(float costopork) {
        this.costopork = costopork;
    }

    public float getKilowattsc() {
        return kilowattsc;
    }

    public void setKilowattsc(float kilowattsc) {
        this.kilowattsc = kilowattsc;
    }
           
     public float calcularSubtotal(){
    
         float subtotal=0.0f;
         switch(this.tiposervicio){
             case 1:
                 this.costopork=2.00f;
                 subtotal=this.kilowattsc*this.costopork;
                 break;
             case 2:
                 this.costopork=3.00f;
                 subtotal=this.kilowattsc*this.costopork;
                 break;
             case 3:
                 this.costopork=3.00f;
                 break;
             default:
                 System.out.println("opcion no valida");
         }
         return subtotal;
    }
                public float calcularImpuesto(){
                    float impuesto=0.0f;
                    impuesto=this.calcularSubtotal()*0.16f;
                    return impuesto;
    }
                 public float calcularTotal(){
        float total=0.0f;
        total=this.calcularSubtotal()+this.calcularImpuesto();
        return total;
    }
    
}