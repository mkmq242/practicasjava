/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cotizacion;



/**
 *
 //ulises//
 */
public class cotizacion {
    // Atributos de la clase
    
    private String descripAuto;
    private float precio;
    private float porcentajeImpuesto;
    private float plazo;
    private String nucotizacion;

    // constructores
    public cotizacion() {
        
        this.nucotizacion="";
        this.descripAuto = "";
        this.precio = 0.0f;
        this.porcentajeImpuesto = 0.0f;
        this.plazo = 0;
    }

    public cotizacion(String nucotizacion, String descripAuto, float precio, float porcentajeImpuesto, float plazo) {
        
        this.descripAuto = descripAuto;
        this.precio = precio;
        this.porcentajeImpuesto = porcentajeImpuesto;
        this.plazo = plazo;
    }


      public String getNucotizacion() {
        return nucotizacion;
    }

    public void setNucotizacion(String nucotizacion) {
        this.nucotizacion = nucotizacion;
    }
    
   
    public String getDescripAuto() {
        return descripAuto;
    }

    public void setDescripAuto(String descripAuto) {
        this.descripAuto = descripAuto;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }

    public double getPorcentajeImpuesto() {
        return porcentajeImpuesto;
    }

    public void setPorcentajeImpuesto(float porcentajeImpuesto) {
        this.porcentajeImpuesto = porcentajeImpuesto;
    }

    public Float getPlazo() {
        return plazo;
    }

    public void setPlazo(int plazo) {
        this.plazo = plazo;
    }

    // metodos de comportamiento
    public float calcularPagoImpuesto(float precio,float PorcentajeImpuesto ) {
        // calcula el impuesto con el precio y el porcentaje de impuesto
        float pi=this.precio*this.porcentajeImpuesto /100;
        return pi;
    }

    public float calcularFinanciamiento(float precio,float PorcentajeImpuesto) {
        // calcula el financiamiento restando el impuesto del precio
        float pi=this.precio*this.porcentajeImpuesto /100;
        float precioF=this.precio-pi;
        
        return precioF;
    }

    public float calcularMensualidad(float precio,float PorcentajeImpuesto,int plazo) {
        // calcula la mensualidad dividiendo el financiamiento entre el plazo
        float pi=this.precio*this.porcentajeImpuesto /100;
        float precioF=this.precio-pi;
        float cm=precioF/this.plazo;
       
        return cm;
    }
}